/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

package io.dyff.kafka.streams

import java.nio.file.Path
import java.util.Properties
import java.util.Base64
import java.util.UUID

import scala.jdk.CollectionConverters._

import org.apache.kafka.common.serialization._
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.TopologyTestDriver

import org.junit.jupiter.api._
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import upickle.default._

import _root_.io.dyff.kafka.streams.ConfigMap
import _root_.io.dyff.kafka.streams.WorkflowsAggregatorTopologyBuilder
import _root_.io.dyff.kafka.streams.implicits.{
  JsonMessageImplicits,
  MsgpackMessageImplicits
}

class MockConfigMap(
    val data: Map[String, String],
    val binaryData: Map[String, String]
) extends ConfigMap {
  def data[T](key: String)(implicit conversion: String => T): Option[T] = {
    data.get(key).map(v => v: T)
  }

  def binaryData[T](
      key: String
  )(implicit conversion: Array[Byte] => T): Option[T] = {
    binaryData.get(key).map(v => Base64.getDecoder().decode(v): T)
  }
}

class WorkflowsStateAppTests {

  def convertBytesToHex(bytes: Seq[Byte]): String = {
    val sb = new StringBuilder
    for (b <- bytes) {
      sb.append(String.format("%02x", Byte.box(b)))
    }
    sb.toString
  }

  def assertEqualsModuloTimestamps(
      expected: ujson.Value,
      actual: ujson.Value
  ): Unit = {
    val expectedMap =
      expected.obj.toMap.removed("creationTime").removed("lastTransitionTime")
    val actualMap =
      actual.obj.toMap.removed("creationTime").removed("lastTransitionTime")
    assertEquals(expectedMap, actualMap)
  }

  @Test
  def serdesJson(): Unit = {
    import JsonMessageImplicits._
    val original: Map[String, ujson.Value] = Map(
      "foo" -> "bar",
      "answer" -> 42,
      "list" -> List(1, 2, 3)
    )
    println(write(original))

    val serialized =
      implicitly[Serializer[ujson.Value]].serialize("unused topic", original)
    println(s"serialized: ${convertBytesToHex(serialized)}")

    val deserialized: Map[String, ujson.Value] =
      implicitly[Deserializer[ujson.Value]]
        .deserialize("unused topic", serialized)
        .obj
        .toMap
    println(s"deserialized: $deserialized")

    assertEquals(deserialized, original)
  }

  @Test
  def serdesMsgpack(): Unit = {
    import MsgpackMessageImplicits._
    val original: Map[String, ujson.Value] = Map(
      "foo" -> "bar",
      "answer" -> 42,
      "list" -> List(1, 2, 3)
    )
    println(write(original))

    val serialized =
      implicitly[Serializer[ujson.Value]].serialize("unused topic", original)
    println(s"serialized: ${convertBytesToHex(serialized)}")

    val deserialized: Map[String, ujson.Value] =
      implicitly[Deserializer[ujson.Value]]
        .deserialize("unused topic", serialized)
        .obj
        .toMap
    println(s"deserialized: $deserialized")

    assertEquals(deserialized, original)
  }

  @ParameterizedTest
  @ValueSource(strings = Array("Json")) // , "Msgpack"))
  def entityEventsToState(serdeFormat: String, @TempDir tempDir: Path): Unit = {
    import org.apache.kafka.streams.scala.serialization.Serdes._

    val configMap = new MockConfigMap(
      data = Map(
        "DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS" -> "workflows.events",
        "DYFF_KAFKA__TOPICS__WORKFLOWS_STATE" -> "workflows.state",
        // "app.topics.input.events" -> "workflows.events",
        // "app.topics.output.state" -> "workflows.state",

        "DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS" -> "dummy config",
        "DYFF_KAFKA__STREAMS__APPLICATION_ID" -> s"workflows-aggregator-app-v0-$serdeFormat",
        "DYFF_KAFKA__STREAMS__STATE_DIR" -> tempDir.toAbsolutePath().toString()
        // "kafka.streams.application.id" -> s"events-to-state-app-v0-$serdeFormat",
        // "kafka.streams.bootstrap.servers" -> "dummy config",
        // "kafka.streams.state.dir" -> tempDir.toAbsolutePath().toString()

        // "DYFF_RESOURCES__DATASETS__STORAGE__URL" -> "s3://dyff/datasets",
        // // DYFF_RESOURCES__INFERENCESERVICES__STORAGE__URL: local.dyff_storage_paths.inferenceservices
        // "DYFF_RESOURCES__MEASUREMENTS__STORAGE__URL" -> "s3://dyff/measurements",
        // "DYFF_RESOURCES__MODELS__STORAGE__URL" -> "s3://dyff/models",
        // "DYFF_RESOURCES__MODULES__STORAGE__URL" -> "s3://dyff/modules",
        // "DYFF_RESOURCES__OUTPUTS__STORAGE__URL" -> "s3://dyff/outputs",
        // "DYFF_RESOURCES__REPORTS__STORAGE__URL" -> "s3://dyff/reports",
        // "DYFF_RESOURCES__SAFETYCASES__STORAGE__URL" -> "s3://dyff/safetycases",
        // "DYFF_STORAGE__S3__ENDPOINT" -> "s3.minio.dyff.local",
        // "DYFF_STORAGE__S3__INTERNAL_ENDPOINT" -> "http://minio.minio.svc.cluster.local:9000",
        // "DYFF_STORAGE__S3__ACCESS_KEY" -> "fake",
        // // s3 client library uses AWS-style env variable names
        // "AWS_ENDPOINT_URL" -> "http://minio.minio.svc.cluster.local:9000",
        // "AWS_ACCESS_KEY_ID" -> "fake",
        // "DYFF_STORAGE__S3__SECRET_KEY" -> "fake",
        // // s3 client library uses AWS-style env variable names
        // "AWS_SECRET_ACCESS_KEY" -> "fake"
      ),
      binaryData = Map()
    )

    val app =
      if (serdeFormat == "Msgpack") {
        new WorkflowsAggregatorTopologyBuilderWithMsgpack(configMap)
      } else if (serdeFormat == "Json") {
        new WorkflowsAggregatorTopologyBuilderWithJson(configMap)
      } else {
        throw new IllegalArgumentException()
      }

    println(s"$serdeFormat: $tempDir")
    val config: Properties = app.kafkaProperties()
    val topology = app.buildTopology()
    val testDriver = new TopologyTestDriver(topology, config)

    val eventsTopic = testDriver.createInputTopic(
      app.eventsTopicName,
      new StringSerializer,
      app.serializer[ujson.Value]
    )
    val stateTopic = testDriver.createOutputTopic(
      app.stateTopicName,
      new StringDeserializer,
      app.deserializer[ujson.Value]
    )
    val kvStore =
      testDriver.getKeyValueStore[String, ujson.Obj](app.stateStoreName)

    val idA: String = UUID.randomUUID().toString
    val idB: String = UUID.randomUUID().toString
    val dataset: String = UUID.randomUUID.toString

    val createEventA: ujson.Obj = ujson.Obj(
      "id" -> idA,
      "kind" -> "Dataset",
      "labels" -> ujson.Obj("name" -> "foobar"),
      "dataset" -> dataset,
      "status" -> "Created"
    )
    eventsTopic.pipeInput(idA, createEventA)
    val createEventAState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $createEventAState")
    assertEqualsModuloTimestamps(createEventA, createEventAState.value)
    val createEventAStateStored: ujson.Obj = kvStore.get(idA)
    assertEqualsModuloTimestamps(
      createEventA,
      createEventAStateStored.value
    )

    val createEventB: ujson.Obj = Map(
      "id" -> idB,
      "kind" -> "Dataset",
      "dataset" -> dataset,
      "status" -> "Created"
    )
    eventsTopic.pipeInput(idB, createEventB)
    val createEventBState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $createEventBState")
    assertEqualsModuloTimestamps(createEventB, createEventBState.value)
    val createEventBStateStored: ujson.Obj = kvStore.get(idB)
    assertEqualsModuloTimestamps(
      createEventB,
      createEventBStateStored.value
    )

    val statusEventA = Map(
      "status" -> "Admitted"
    )
    eventsTopic.pipeInput(idA, statusEventA)
    val statusEventAState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $statusEventAState")
    val statusEventAStateTruth = createEventA.obj.clone()
    statusEventAStateTruth.put("status", statusEventA.get("status").get)
    assertEqualsModuloTimestamps(
      statusEventAStateTruth,
      statusEventAState.value
    )
    val statusEventAStateStored: ujson.Obj = kvStore.get(idA)
    assertEqualsModuloTimestamps(
      statusEventAStateTruth,
      statusEventAStateStored.value
    )

    // Test the new command schema
    val statusEventB = ujson.Obj(
      "command" -> "UpdateEntityStatus",
      "body" -> ujson.Obj(
        "status" -> "Failed"
      )
    )
    eventsTopic.pipeInput(idB, statusEventB)
    val statusEventBState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $statusEventBState")
    val statusEventBStateTruth = createEventB.obj.clone()
    statusEventBStateTruth.put(
      "status",
      statusEventB.obj.get("body").get.obj.get("status").get
    )
    assertEqualsModuloTimestamps(
      statusEventBStateTruth,
      statusEventBState.value
    )
    val statusEventBStateStored: ujson.Obj = kvStore.get(idB)
    assertEqualsModuloTimestamps(
      statusEventBStateTruth,
      statusEventBStateStored.value
    )

    val labelsEventA = ujson.Obj(
      "labels" -> ujson.Obj("project" -> "jenova")
    )
    eventsTopic.pipeInput(idA, labelsEventA)
    val labelsEventAState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $labelsEventAState")
    val labelsEventAStateTruth = ujson.Obj(
      "id" -> idA,
      "kind" -> "Dataset",
      "labels" -> ujson.Obj("name" -> "foobar", "project" -> "jenova"),
      "dataset" -> dataset,
      "status" -> "Admitted"
    )
    assertEqualsModuloTimestamps(
      labelsEventAStateTruth,
      labelsEventAState.value
    )
    val labelsEventAStateStored: ujson.Obj = kvStore.get(idA)
    assertEqualsModuloTimestamps(
      labelsEventAStateTruth,
      labelsEventAStateStored.value
    )

    val overwriteLabelsEventA = ujson.Obj(
      "labels" -> ujson.Obj("project" -> "crm-114")
    )
    eventsTopic.pipeInput(idA, overwriteLabelsEventA)
    val overwriteLabelsEventAState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $overwriteLabelsEventAState")
    val overwriteLabelsEventAStateTruth = ujson.Obj(
      "id" -> idA,
      "kind" -> "Dataset",
      "labels" -> ujson.Obj("name" -> "foobar", "project" -> "crm-114"),
      "dataset" -> dataset,
      "status" -> "Admitted"
    )
    assertEqualsModuloTimestamps(
      overwriteLabelsEventAStateTruth,
      overwriteLabelsEventAState.value
    )
    val overwriteLabelsEventAStateStored: ujson.Obj = kvStore.get(idA)
    assertEqualsModuloTimestamps(
      overwriteLabelsEventAStateTruth,
      overwriteLabelsEventAStateStored.value
    )

    val labelNoneEventA = ujson.Obj(
      "labels" -> ujson.Obj("project" -> ujson.Null)
    )
    eventsTopic.pipeInput(idA, labelNoneEventA)
    val labelNoneEventAState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $labelNoneEventAState")
    val labelNoneEventAStateTruth = ujson.Obj(
      "id" -> idA,
      "kind" -> "Dataset",
      "labels" -> ujson.Obj("name" -> "foobar", "project" -> ujson.Null),
      "dataset" -> dataset,
      "status" -> "Admitted"
    )
    assertEqualsModuloTimestamps(
      labelNoneEventAStateTruth,
      labelNoneEventAState.value
    )
    val labelNoneEventAStateStored: ujson.Obj = kvStore.get(idA)
    assertEqualsModuloTimestamps(
      labelNoneEventAStateTruth,
      labelNoneEventAStateStored.value
    )

    // Tombstone
    val tombstoneEventA = ujson.Obj(
      "command" -> "ForgetEntity"
    )
    eventsTopic.pipeInput(idA, tombstoneEventA)
    val tombstoneEventAState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $tombstoneEventAState")
    assertEquals(null, tombstoneEventAState.value)
    val tombstoneEventAStateStored: ujson.Obj = kvStore.get(idA)
    println(tombstoneEventAStateStored)
    assertEquals(
      null,
      tombstoneEventAStateStored
    )

    // Other entity should still be there
    val aliveEventB = Map(
      "status" -> "TestStillAlive"
    )
    eventsTopic.pipeInput(idB, aliveEventB)
    val aliveEventBState = stateTopic.readKeyValue()
    println(s"$serdeFormat: $aliveEventBState")
    val aliveEventBStateTruth = createEventB.obj.clone()
    aliveEventBStateTruth.put("status", aliveEventB.get("status").get)
    assertEqualsModuloTimestamps(
      aliveEventBStateTruth,
      aliveEventBState.value
    )
    val aliveEventBStateStored: ujson.Obj = kvStore.get(idB)
    assertEqualsModuloTimestamps(
      aliveEventBStateTruth,
      aliveEventBStateStored.value
    )

    testDriver.close()
  }
}

object EntityStateTests {}
