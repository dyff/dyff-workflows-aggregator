/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

package io.dyff.kafka.streams

import java.io.File
import java.nio.charset.StandardCharsets
import java.time.Duration
import java.util.Base64
import java.util.Properties

import scala.jdk.CollectionConverters._
import scala.util.{Properties => ScalaProperties}

import io.minio.messages.DeleteObject
import io.minio.ListObjectsArgs
import io.minio.MinioClient
import io.minio.RemoveObjectArgs
import io.minio.RemoveObjectsArgs

import org.apache.commons.io.FileUtils
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream._

import org.slf4j.{Logger, LoggerFactory}

import _root_.io.dyff.kafka.streams.implicits.JsonMessageImplicits
import _root_.io.dyff.kafka.streams.implicits.MessageImplicits
import _root_.io.dyff.kafka.streams.implicits.MsgpackMessageImplicits
import java.time.ZonedDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

/** Represents a Kubernetes `ConfigMap` resource.
  */
trait ConfigMap {

  /** Return a value in the `data` section, applying an implicit conversion.
    */
  def data[T](key: String)(implicit conversion: String => T): Option[T]

  /** Return a value in the `binaryData` section, applying an implicit
    * conversion.
    */
  def binaryData[T](key: String)(implicit
      conversion: Array[Byte] => T
  ): Option[T]
}

/** A `ConfigMap` where the values have been mounted as files in `/etc/config`.
  */
class MountedConfigMap extends ConfigMap {
  val configDir: File = new File("/etc/config")

  val configMap: Map[String, String] = {
    configDir.listFiles
      .map(f =>
        f.getName() -> FileUtils.readFileToString(f, StandardCharsets.UTF_8)
      )
      .toMap
  }

  def data[T](key: String)(implicit conversion: String => T): Option[T] = {
    configMap.get(key).map(v => v: T)
  }

  def binaryData[T](
      key: String
  )(implicit conversion: Array[Byte] => T): Option[T] = {
    configMap.get(key).map(v => Base64.getDecoder().decode(v): T)
  }
}

/** A `ConfigMap` where the values have been injected as environment variables
  * using the `envFrom` option.
  */
class EnvironmentVariableConfigMap extends ConfigMap {
  def data[T](key: String)(implicit conversion: String => T): Option[T] = {
    ScalaProperties.envOrNone(key).map(v => v: T)
  }

  def binaryData[T](
      key: String
  )(implicit conversion: Array[Byte] => T): Option[T] = {
    ScalaProperties.envOrNone(key).map(v => Base64.getDecoder().decode(v): T)
  }
}

class DyffStorageConfig(configMap: ConfigMap) {
  val endpoint =
    configMap.data[String]("DYFF_STORAGE__S3__INTERNAL_ENDPOINT").get

  val accessKey = configMap.data[String]("DYFF_STORAGE__S3__ACCESS_KEY").get
  val secretKey = configMap.data[String]("DYFF_STORAGE__S3__SECRET_KEY").get

  val urls = new {
    val datasets =
      configMap.data[String]("DYFF_RESOURCES__DATASETS__STORAGE__URL").get
    val evaluations =
      configMap.data[String]("DYFF_RESOURCES__OUTPUTS__STORAGE__URL").get
    val measurements =
      configMap.data[String]("DYFF_RESOURCES__MEASUREMENTS__STORAGE__URL").get
    val models =
      configMap.data[String]("DYFF_RESOURCES__MODELS__STORAGE__URL").get
    val modules =
      configMap.data[String]("DYFF_RESOURCES__MODULES__STORAGE__URL").get
    val reports =
      configMap.data[String]("DYFF_RESOURCES__REPORTS__STORAGE__URL").get
    val safetycases =
      configMap.data[String]("DYFF_RESOURCES__SAFETYCASES__STORAGE__URL").get
  }

  def url(kind: String): String = {
    kind match {
      case "Dataset"     => this.urls.datasets
      case "Evaluation"  => this.urls.evaluations
      case "Measurement" => this.urls.measurements
      case "Model"       => this.urls.models
      case "Module"      => this.urls.modules
      case "Report"      => this.urls.reports
      case "SafetyCase"  => this.urls.safetycases
    }
  }
}

object CommandSchema {
  val CreateEntity = "CreateEntity"
  val EditEntityDocumentation = "EditEntityDocumentation"
  val ForgetEntity = "ForgetEntity"
  val UpdateEntityStatus = "UpdateEntityStatus"
}

/** Aggregates workflow events into the materialized current state of each
  * entity in the workflow.
  *
  * The following environment variables are used for configuration:
  *
  * <table> <tr> <td>DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS</td> <td>The name of
  * the workflows events topic. The app consumes from this topic.</td> </tr>
  * <tr> <td>DYFF_KAFKA__TOPICS__WORKFLOWS_STATE</td> <td>The name of the
  * workflows state topic. The app produces to this topic.</td> </tr> <tr>
  * <td>DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS</td> <td>The Kafka
  * `bootstrap.servers` property.</td> </tr> <tr>
  * <td>DYFF_KAFKA__STREAMS__APPLICATION_ID</td> <td>The Kafka Streams
  * `application.id` property.</td> </tr> <tr>
  * <td>DYFF_KAFKA__STREAMS__STATE_DIR</td> <td>The Kafka Streams `state.dir`
  * property.</td> </tr> </table>
  *
  * @constructor
  *   Create a topology builder with the specified config options.
  * @param configMap
  *   A [[io.dyff.kafka.streams.ConfigMap]] mapping environment variables to
  *   values.
  */
abstract class WorkflowsAggregatorTopologyBuilder(val configMap: ConfigMap)
    extends MessageImplicits {
  val logger: Logger =
    LoggerFactory.getLogger(classOf[WorkflowsAggregatorTopologyBuilder])
  val eventsTopicName: String =
    configMap.data("DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS").get
  val stateTopicName: String =
    configMap.data("DYFF_KAFKA__TOPICS__WORKFLOWS_STATE").get
  val stateStoreName: String = stateTopicName

  val storageConfig: Option[DyffStorageConfig] = {
    configMap.data[String]("DYFF_STORAGE__S3__INTERNAL_ENDPOINT") match {
      case Some(_) => Some(new DyffStorageConfig(configMap))
      case None    => None
    }
  }

  val s3client: Option[MinioClient] = {
    storageConfig match {
      case Some(config) =>
        Some(
          MinioClient
            .builder()
            .endpoint(config.endpoint)
            .credentials(config.accessKey, config.secretKey)
            .build()
        )
      case None => None
    }
  }

  private def mergeValues(
      base: Option[ujson.Value],
      update: Option[ujson.Value]
  ): ujson.Value = {
    (base, update) match {
      // Value exists in both inputs
      case (Some(b), Some(u)) =>
        (b.objOpt, u.objOpt) match {
          // If both values are Objects, merge them recursively
          case (Some(x), Some(y)) => mergeObj(x, y)
          // Otherwise, take the 'update' value
          case _ => u
        }
      // Invariant: Never both None because of how mergeObj is implemented
      case _ => (update orElse base).get
    }
  }

  private def mergeObj(base: ujson.Obj, update: ujson.Obj): ujson.Obj = {
    // Create a new Object by merging the (k -> v) pairs in both inputs
    (base.obj.keySet ++ update.obj.keySet).map(key =>
      key -> mergeValues(base.obj.get(key), update.obj.get(key))
    )
  }

  private def dropProtocol(path: String): String = {
    val protocol = "s3://"
    if (path.startsWith(protocol)) {
      path.substring(protocol.length())
    } else {
      path
    }
  }

  private def splitBucketPath(path: String): (String, String) = {
    val stripped = dropProtocol(path)
    val parts = stripped.split("/")
    return (parts(0), parts.drop(1).mkString("/"))
  }

  private def forgetArtifacts(entity: ujson.Obj): Unit = {
    (s3client, storageConfig) match {
      case (Some(client), Some(config)) =>
        forgetArtifactsImpl(client, config, entity)
      case _ => ()
    }
  }

  private def forgetArtifactsImpl(
      s3client: MinioClient,
      storageConfig: DyffStorageConfig,
      entity: ujson.Obj
  ): Unit = {
    logger.info("forgetArtifacts(): {}", entity.obj)
    val kind = entity("kind").str
    val id = entity("id").str

    // Configured URL can be a sub-path within a bucket, so we construct the
    // full path and then split it
    val root = storageConfig.url(kind)
    val path = root + "/" + id
    val (bucket, obj) = splitBucketPath(path)

    val listArgs = ListObjectsArgs
      .builder()
      .bucket(bucket)
      .prefix(obj)
      .recursive(true)
      .build()
    val objects = s3client.listObjects(listArgs).asScala.map(item => item.get())
    for (obj <- objects) {
      logger.info(
        "key={}, command=ForgetEntity: Forgetting artifact {}",
        id,
        obj.objectName()
      )
      // FIXME: (DYFF-584) GCS doesn't support batch delete via the s3 API.
      s3client.removeObject(
        RemoveObjectArgs
          .builder()
          .bucket(bucket)
          .`object`(obj.objectName())
          .build()
      )
    }
  }

  private def now(): String = {
    ZonedDateTime
      .now(ZoneOffset.UTC)
      .format(DateTimeFormatter.ISO_INSTANT)
  }

  private def applyCommandMessage(
      key: String,
      msg: ujson.Obj,
      state: ujson.Obj
  ): ujson.Obj = {
    val command = msg("command").str
    command match {
      // TODO: For v1, we will validate all of these against a command
      // schema and not just blindly patch the state.
      case CommandSchema.CreateEntity => {
        if (state.obj.isEmpty) {
          // Empty is the "init" state for the .aggregate() operation
          val newState = msg("body").obj
          newState("lastTransitionTime") = now()
          newState
        } else {
          logger.warn(
            "key={}, command={}: Entity exists; ignoring command",
            key,
            command
          )
          state
        }
      }
      case CommandSchema.EditEntityDocumentation => {
        if (!state.obj.isEmpty) {
          mergeObj(state, msg("body").obj)
        } else {
          logger.warn(
            "key={}, command={}: Entity does not exist; ignoring command",
            key,
            command
          )
          state
        }
      }
      case CommandSchema.ForgetEntity => {
        forgetArtifacts(state)
        null
      }
      case CommandSchema.UpdateEntityStatus => {
        if (!state.obj.isEmpty) {
          val newState = mergeObj(state, msg("body").obj)
          newState("lastTransitionTime") = now()
          newState
        } else {
          logger.warn(
            "key={}, command={}: Entity does not exist; ignoring command",
            key,
            command
          )
          state
        }
      }
      // case "JsonMergePatch" => mergeObj(msg("body").obj, state)
    }
  }

  /** Construct the Kafka Streams processing Topology.
    */
  def buildTopology(): Topology = {
    import org.apache.kafka.streams.scala.serialization.Serdes._
    import org.apache.kafka.streams.scala.ImplicitConversions._

    val builder: StreamsBuilder = new StreamsBuilder()
    val entityEventsStream =
      builder.stream[String, ujson.Obj](eventsTopicName)
    val entityStateTable = entityEventsStream.groupByKey
      .aggregate[ujson.Obj](ujson.Obj())(
        (key: String, event: ujson.Obj, state: ujson.Obj) => {
          event.obj.get("command") match {
            case Some(_) => applyCommandMessage(key, event, state)
            // This event pre-dates the event schema
            // Fall back to old behavior
            case None => mergeObj(state, event)
          }
        }
      )(Materialized.as(stateStoreName))
    entityStateTable.toStream.to(stateTopicName)
    builder.build()
  }

  def kafkaProperties(): Properties = {
    // This should always be exactly_once_v2; raise an error if user tries to
    // set it to something else.
    configMap
      .data[String]("DYFF_KAFKA__STREAMS__PROCESSING_GUARANTEE")
      .map(v => {
        if (v != StreamsConfig.EXACTLY_ONCE_V2) {
          throw new IllegalArgumentException(
            s"DYFF_KAFKA__STREAMS__PROCESSING_GUARANTEE must be ${StreamsConfig.EXACTLY_ONCE_V2}"
          )
        }
      })

    val p = new Properties()
    p.put(
      StreamsConfig.PROCESSING_GUARANTEE_CONFIG,
      StreamsConfig.EXACTLY_ONCE_V2
    )
    p.put(
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
      configMap.data[String]("DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS").get
    )
    p.put(
      StreamsConfig.APPLICATION_ID_CONFIG,
      configMap.data[String]("DYFF_KAFKA__STREAMS__APPLICATION_ID").get
    )
    p.put(
      StreamsConfig.STATE_DIR_CONFIG,
      configMap.data[String]("DYFF_KAFKA__STREAMS__STATE_DIR").get
    )
    p.put(
      StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
      "org.apache.kafka.streams.errors.LogAndContinueExceptionHandler"
    )
    // TODO: Probably at least 2 threads, depending on k8s resource config
    // p.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, 2)
    p
  }
}

/** EntityStateApp using JSON string encoding for messages.
  */
class WorkflowsAggregatorTopologyBuilderWithJson(configMap: ConfigMap)
    extends WorkflowsAggregatorTopologyBuilder(configMap)
    with JsonMessageImplicits {}

/** EntityStateApp using Msgpack encoding for messages.
  */
class WorkflowsAggregatorTopologyBuilderWithMsgpack(configMap: ConfigMap)
    extends WorkflowsAggregatorTopologyBuilder(configMap)
    with MsgpackMessageImplicits {}

object WorkflowsAggregatorApp extends App {
  val configMap: ConfigMap = new EnvironmentVariableConfigMap()
  val builder = new WorkflowsAggregatorTopologyBuilderWithJson(configMap)

  // TODO: Kubernetes bootstrap server
  // TODO: PVC mount point for persistence
  val config: Properties = builder.kafkaProperties()
  val topology = builder.buildTopology()

  val streams: KafkaStreams = new KafkaStreams(topology, config)
  streams.start()

  sys.ShutdownHookThread {
    streams.close(Duration.ofSeconds(10))
  }
}
