# dyff/workflows-aggregator

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/dyff/workflows-aggregator?branch=main)](https://gitlab.com/dyff/workflows-aggregator/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/dyff/workflows-aggregator)](https://gitlab.com/dyff/workflows-aggregator/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools Enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Compute the `workflows.state` Kafka stream from the `workflows.events` log.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Getting started

First, head to
[dyff/dev-environment](https://gitlab.com/dyff/dev-environment) to set up a
local Kubernetes cluster specially prepared for dyff development.

Once your cluster is ready to go, use `skaffold` to spin up an instance of
`workflows-aggregator`:

```bash
skaffold dev --namespace dyff
```

When you see this message, you'll know the instance has stabilized:

```
Deployments stabilized in 5.077 seconds
Listing files to watch...
 - us-central1-docker.pkg.dev/dyff-354017/dyff-system/dyff/workflows-aggregator
Press Ctrl+C to exit
Watching for changes...
```

## Configuration

The following environment variables are available for configuration:

- **`DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS`**:

  ```yaml
  DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS: dyff-kafka-bootstrap:9092
  ```

- **`DYFF_KAFKA__STREAMS__APPLICATION_ID`**:

  ```yaml
  DYFF_KAFKA__STREAMS__APPLICATION_ID: workflows-aggregator-app-v0
  ```

- **`DYFF_KAFKA__STREAMS__STATE_DIR`**:

  ```yaml
  DYFF_KAFKA__STREAMS__STATE_DIR: /kafka/state
  ```

- **`DYFF_KAFKA__STREAMS__PROCESSING_GUARANTEE`**:

- **`DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS`**:

  ```yaml
  DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS: dyff.workflows.events
  ```

- **`DYFF_KAFKA__TOPICS__WORKFLOWS_STATE`**:

  ```yaml
  DYFF_KAFKA__TOPICS__WORKFLOWS_STATE: dyff.workflows.state
  ```

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
